/**
 * This macro requires the About Time module and is designed for the PF1e system
 * Creates an event that triggers after the duration expires
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 *  (based on a macro by an Lupusmalus#3187)
 * This macro was modified from a macro from an Lupusmalus#3187, 
 *  much of what it does is a holdover from them and only modified by me, so I don't understand everything it does
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {

const CL = token.actor.data.data.attributes.spells.spellbooks.primary.cl.total; //Sets the constant CL based on tokens primary caster level
let dialogContentlabel = `<div><span style="flex:1">Spell Name: <input name="label" style="width:350px"/></span></div>`, //text box for inputting spell name
    dialogContentduration = `<div><span style="flex:1">Custom Duration in Minutes (leave blank for none): <input name="duration" style="width:350px"/></span></div>`, //text box for inputting duration
    d = new Dialog({                                        //*
title: "Enter Spell Info",                                  //*
content: dialogContentlabel + dialogContentduration,        //*
buttons: {                                                  //*
    done: {                                                 //*
        label: "Change!",                                   //creates and defines the dialog box
        callback: (html) => {
            if (isNaN(parseFloat(html.find("[name=duration]")[0].value))) //determines if an integer was input for custom duration
                {spellDurationNotification(html.find("[name=label]")[0].value, CL);}   //if not, sets the custom duration same as one of the default, causing it to not be shown
            else 
                {spellDurationNotification(html.find("[name=label]")[0].value, parseFloat(html.find("[name=duration]")[0].value));} //if it is an int, sends it to the spell duration notification function
        }
    },
},
default: "done"    
});
d.render(true);

function spellDurationNotification(spellLabel, spellDuration) {
    const durationLengths = [1*CL+" minutes", 10*CL+" minutes", 1*CL+" hours","24 hours",1*CL+" days",spellDuration+" minutes"]; //sets the names of all the durations
    const durationNum = [1*CL, 10*CL, 60*CL, 1440,1440*CL, spellDuration]; //sets the durations in minutes
    const text = '<div style="width:100%; text-align:center;">' + "Select spell duration:" + '</div>'; //creates the text box

    //beyond this point I don't really understand what it's doing, almost all this is unmodified from the original macro
    let buttons = {}, dialog, content = text;

    durationLengths.forEach((str)=> {
        buttons[str] = {
            label : str,
            callback : () => {
            const targetTime = window.SimpleCalendar.api.timestampToDate(game.time.worldTime);
            targetTime.minute += durationNum[durationLengths.indexOf(str)];
            sendChatMsg("In " + str + ", this spell, " + spellLabel + ", will end!", str);
            const endedMsg = msgFormat(actor.getActiveTokens()[0].name + "'s spell, " + spellLabel + ", ended", "The spell that was cast " + str + " ago, " + spellLabel + ", has ended by now.", str);
            const spellDurationId = game.Gametime.reminderAt(targetTime, endedMsg);
            // actor.setFlag("dnd5e","spellDurationId",spellDurationId);
            console.log("Spell Tracker: added spell id", spellDurationId, "at", game.Gametime.getTimeString(),"(in-game time)");
            close: html => dialog.render(true); //modified this line so the dialog closes after a selection is made
            }
        }
        });

    dialog = new Dialog({title : 'Set spell duration', content, buttons}).render(true);
    };

    function msgFormat(isActiveMsg, msgContent, durationText) {
    const htmlMsg = '<div><div class="dnd5e chat-card item-card">\
                    <header class="card-header flexrow red-header">\
                        <img src="icons/tools/navigation/hourglass-yellow.webp" title="Spell Tracker" width="72" height="72">\
                        <h3 class="item-name">' + isActiveMsg + '</h3>\
                    </header>\
                    <div class="card-content br-text" style="display: block;">\
                        <p>' + msgContent + '</p></div>\
                        <footer class="card-footer">\
                        <span>Spell Duration Tracker</span>\
                        <span>' + durationText + '</span>\
                        <span>' + actor.name + '\
                        </footer>\
                    </div>';
    return htmlMsg; //modified the hourglass size to fit PF1e better
    }

    function sendChatMsg(msgContent, durationText) {
    const chatData = {
        user:  game.user.id,
        speaker: game.user,
        content: msgFormat(actor.getActiveTokens()[0].name + " is casting a Spell", msgContent, durationText),
        // Uncomment the line below this if you want the message to be whispered to the GM
        //whisper: game.users.entities.filter(u => u.isGM).map(u => u._id)
    };
    ChatMessage.create(chatData,{});
    };
}