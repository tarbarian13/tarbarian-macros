const c = {
	actorNames: [],
	warningMissingActors: "No applicable actor(s) found"
};
// END CONFIGURATION

//Set up targets and verify they can be edited
const myTokens = canvas.tokens.controlled;
let myActors,
	preselected;
if (c.actorNames.length > 0)
	myActors = game.actors.filter(o => c.actorNames.includes(o.name));
else
	myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.isOwner);
if (typeof state !== "undefined") { 
	if (typeof token !== "undefined") myActors = [token.actor];
	else if (typeof actor !== "undefined") myActors = [actor];
}

var conditions = [];
for (const [key] of Object.entries(CONFIG.SFRPG.conditions)) {
  conditions.push(`${key}`);
}
if (!myActors.length) ui.notifications.warn(c.warningMissingActors);
else {
const input = [{label:'Select condition to apply',type:'select',options:conditions},{label:'Enter duration (leave blank for indefinite)',type:'number'}];
const result = await warpgate.menu({
		inputs: input,
		buttons: [{
		  label: 'Apply',
		  value: 1
		}, {
		  label: 'Remove',
		  value: 2
		}, {
		  label: 'Toggle',
		  value: 3
		}]
	  }, {
		options: {
		  width: '100px',
		  height: '100%'
		}
	  });
let results = result['inputs'],
	  cond = results[0]
	  duration = results[1],
	  choice = result['buttons'];
if(isNaN(duration) || duration === "0"){
	if (choice === 1){
	myActors.forEach(o => {
		o.setCondition(cond,true);
	});
	}
	if (choice === 2){
	myActors.forEach(o => {
		o.setCondition(cond,false);
	});
	}
	if (choice === 3){
	myActors.forEach(o => {
		let bool = o.hasCondition(cond)
		bool = !bool
		o.setCondition(cond,bool);
	});
	}  
} else {
	if (choice != 1){ui.notifications.warn("Duration only works when applying conditions")}
	else {
		myActors.forEach(o => {
			o.setCondition(cond,true);
		});
		let alertData = {label: ""};
		const names = [],
			tokenIds = [];
        myActors.forEach(o => {
            names.push(o.name);
        });
		alertData.label = cond+" will expire on "+names;        //changes the label of alertData                                 
        alertData.round = parseInt(duration);                           //sets the duration of alert Data
        alertData.roundAbsolute = false;                                  //sets round absolute as false, meaning the alert triggers a number of rounds after it was created
        alertData.turnId = game.combat.combatant.id;                     //sets the alert to be on the current turn
        alertData.macro = 'removeCondition';   //sets the macro to be triggered on end, null for none
		myTokens.forEach(o => {
			tokenIds.push(o.id);
		});
		alertData.args = [cond, tokenIds];
        alertData.message = cond + " on " + names + " has worn off."; 
		let recipients = [];
        recipients = recipients.concat(game.users.filter((u) => u.isGM).map((u) => u.data._id)); //adds the GM to the list of recipients
        alertData.recipientIds = recipients; //adds all the recipients to the alertData object
        TurnAlert.create(alertData);
	};
}
}