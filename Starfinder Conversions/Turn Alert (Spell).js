/**
 * This macro requires the Turn Alert module and was designed for the PF1e system, converted for Starfinder. Other Turn Alert macros work correctly, except Turn Alert (Condition)
 * Creates an alert that triggers based on the caster level of 
 *  selected token
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 *  (based on a macro by Cole Schultz (cole#9640))
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 * For other Turn Alert example macros and api reference,
 *   visit the Turn Alert wiki:
 *   https://github.com/schultzcole/FVTT-Turn-Alert/wiki
 */
 if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {
    var recipients = []; //creates an empty array for the recipients of the macro
    let alertData = {label: ""}, //creates the alertData object
        dialogContentlabel = `<div><span style="flex:1">Spell name: <input name="label" style="width:350px"/></span></div>`, //text box for inputting spell name
        dialogContentpublic = `<label>Public?</label><input type="checkbox" name="public">`, //checkbox for determining if the message should be public
        dialogContentowner = `<label>To owner?</label><input type="checkbox" name="owner">`, //checkbox for determining if the message should sent to the owner of the token
        dialogContentid = `<label>On selected tokens turn?</label><input type="checkbox" name="id" checked>`, //checkbox for determining where in the turn order alert is created
        d = new Dialog({                                                            //*
        title: "Enter spell name",                                                  //*
        content: dialogContentlabel + dialogContentpublic + dialogContentowner +    //*
            dialogContentid,                                                        //*
        buttons: {                                                                  //*
            done: {                                                                 //*
                label: "Confirm",                                                   //creating and defining the dialog
                callback: (html) => {
                    let myLabel = html.find("[name=label]")[0].value,       //assigns the name based on input text
                        isPublic = html.find("[name=public]")[0].checked,   //creates Boolean if public checkbox is checked
                        toOwner = html.find("[name=owner]")[0].checked,     //creates Boolean if to owner checkbox is checked
                        selectedTurnid = html.find("[name=id]")[0].checked, //'' '' '' id '' '' ''
                        casterLevel = getProperty(token.actor.data,`data.details.cl.value`);
                    alertData.label = myLabel;                               //changes the label of alertData
                    if (casterLevel === 0 || undefined){
                        ui.notifications.warn('Caster Level Type 0 or Undefined!');
                    } else {
                        alertData.round = casterLevel;      //'' '' round '' ''
                        alertData.roundAbsolute = false;                                                             //sets round absolute as false, meaning the alert triggers a number of rounds after it was created
                        if (selectedTurnid) {
                            alertData.turnId = game.combat.turns.find(turn => turn.data.tokenId === token.data._id).id;  //sets the alert to be on the selected tokens turn
                            } else {
                            alertData.turnId = game.combat.combatant.id;  //sets the alert to be on the current turn
                            }
                        alertData.message = myLabel + " ends";                                                 //sets the end message
                        alertData.macro = null;                                                                      //sets the macro to be triggered on end, null for none
                        recipients = recipients.concat(game.users.filter((u) => u.isGM).map((u) => u.data._id));        //adds the GM to the list of recipients
                        if (toOwner) {
                            recipients = recipients.concat(game.users.filter(u => token.actor.hasPerm(u, CONST.ENTITY_PERMISSIONS.OWNER)).map((u) => u.data._id));  //adds the owner to recipients if to owner checked
                            }
                        if (isPublic) {
                            recipients = []; //clears recipients if public is checked, sending the message to everyone.
                            }
                        alertData.recipientIds = recipients; //adds all the recipients to the alertData object
                        TurnAlert.create(alertData); //creates the alert
                    }
                    }
            },
        },
        default: "done"    
    });
    d.render(true);
}