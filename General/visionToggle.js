/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * Intended for use in PF1e and requires the module Warp Gate. Toggles/sets various vision types for use in the PF1e system.
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

async function updateSight(sense,state1,state2) {
let sight = foundry.utils.getProperty(actor,sense);
for (let t of canvas.tokens.controlled) {
	await t.actor.update({[sense]: sight === state2 ? state1 : state2});
};
};


if (!token) {
	ui.notifications.warn("You must select a token!");
} else {
    const dialog = await warpgate.menu({
        inputs: [{
            label: "Enter a range (60 assumed)",
            type: "number",
            options: 60
        }],
        buttons: [{
            label: 'Low-light Vision',
            value: 1
        }, {
            label: 'Darkvision',
            value: 2
        }, {
            label: 'Tremorsense',
            value: 3
        }, {
            label: 'Blindsense',
            value: 4
        }, {
            label: 'Blindsight',
            value: 5
        }, {
            label: 'See Invisibility',
            value: 6
        }, {
            label: 'Truesight',
            value: 7
        }]
    }, {
    title: 'Select a vision type to toggle',
    options: {
        width: '100px',
        height: '100%',    
    }
    });
    let choice = dialog['buttons'],
        input = dialog['inputs'];
   if (choice === false) {}
    else {
    let range = input[0]
    let sense="system.traits.senses."
    switch(choice) {
        case 1:
            sense=sense.concat("ll");
            updateSight(sense,false,true);
            break
        case 2:
            sense=sense.concat("dv");
            updateSight(sense,0,range);
            break
        case 3:
            sense=sense.concat("ts");
            updateSight(sense,0,range);
            break
        case 4:
            sense=sense.concat("bse");
            updateSight(sense,0,range);
            break
        case 5:
            sense=sense.concat("bs");
            updateSight(sense,0,range);
            break
        case 6:
            sense=sense.concat("si");
            updateSight(sense,false,true);
            break
        case 7:
            sense=sense.concat("tr");
            updateSight(sense,false,true);
            break
    }
}
}
