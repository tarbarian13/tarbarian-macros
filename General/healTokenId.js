var tokenId = args[0],
    amount = args[1],
    updateData = {};
const actor = canvas.tokens.placeables.find(o => o.id === tokenId)?.actor;
let currentHp = parseInt(getProperty(actor.getRollData(), "attributes.hp.value")),
    maxHp = parseInt(getProperty(actor.getRollData(), "attributes.hp.max")),
    newHp = currentHp + amount
if (newHp >= maxHp) {
    updateData[`data.attributes.hp.value`] = maxHp;
    canvas.tokens.get(tokenId)?.actor.update(updateData);
} else {
    updateData[`data.attributes.hp.value`] = newHp;
    canvas.tokens.get(tokenId)?.actor.update(updateData);
}