/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * Intended for use with Pathfinder 1st edition, but should work elsewhere. Has options for the common lights available in the system.
 * REQUIRES Warp Gate to function: https://github.com/trioderegion/warpgate 
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */
const c = {
	warningMissingActors: "No applicable actor(s) found"
};
// END CONFIGURATION

//Set up targets and verify they can be edited
function tokenUpdate(data) {
    canvas.tokens.controlled.map(token => token.document.update({light: data}));
};

const myTokens = canvas.tokens.controlled;
let myActors,
	lightTypes=['Torch','Hooden Lantern (Open)','Hooded Lantern (Closed)','Bullseye Lantern','Light','Daylight','Candle','Dusk Lantern'];
myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.isOwner);

if (!myActors.length) ui.notifications.warn(c.warningMissingActors);
else {
const dialog = await warpgate.menu({
    inputs: [{
        label: 'Torch',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Hooded Lantern (Open)',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Hooded Lantern (Closed)',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Bullseye Lantern',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Light',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Daylight',
        type: 'radio',
        options: 'group1'
    }, {
        label: 'Candle',
        type: 'radio',
        options: 'group1'
    },{
        label: 'Dusk Lantern',
        type: 'radio',
        options: 'group1'
    }],
    buttons: [{
        label: 'Light',
        value: 1
    }, {
        label: 'Extinguish',
        value: 2
    }]
  }, {
    title: 'Choose Light Option',
    options: {
      width: '100px',
      height: '100%',    
    }
  });
  let choices = dialog['inputs'];
  if (choices != null) {
  let button = dialog['buttons'],
  choice = lightTypes[choices.indexOf(true)],
  torchAnimation = {"type": "torch", "speed": 2, "intensity": 2, "reverse": false},
  blankAnimation = {"type": "", "speed": 5, "intensity": 5, "reverse": false};
  console.log(choice)
  if (button === 2){
    tokenUpdate({"dim": 0, "bright": 0, "angle": 360, "alpha": 0.5, 'color': "", "animation": blankAnimation})
  } else {
    switch (choice) {
        case 'Torch':
            tokenUpdate({"dim": 40, "bright": 20, "angle": 360, "alpha": 0.05, "animation": torchAnimation, "color": '#c26824'});
            break;
        case 'Hooded Lantern (Open)':
            tokenUpdate({"dim": 60, "bright": 30, "angle": 360, "alpha": 0.05, "animation": torchAnimation, "color": '#c26824'});
            break;
        case 'Hooded Lantern (Closed)':
            tokenUpdate({"dim": 5, "bright": 0, "angle": 360, "alpha": 0.05, "animation": torchAnimation, "color": '#c26824'});
            break;
        case 'Bullseye Lantern':
            tokenUpdate({"dim": 120, "bright": 60, "angle": 60, "alpha": 0.05, "animation": torchAnimation, "color": '#c26824'});
            break;
        case 'Light':
            tokenUpdate({"dim": 40, "bright": 20, "angle": 360, "alpha": 0.05, "animation": blankAnimation, "color": '#dbdbdb'});
            break;
        case 'Daylight':
            tokenUpdate({"dim": 120, "bright": 60, "angle": 360, "alpha": 0.05, "animation": blankAnimation, "color": '#dbdbdb'});
            break;
        case 'Candle':
            tokenUpdate({"dim": 10, "bright": 5, "angle": 360, "alpha": 0.05, "animation": torchAnimation, "color": '#c26824'});
            break;
        case 'Dusk Lantern':
            tokenUpdate({"dim": 120, "bright": 60, "angle": 60, "alpha": 0.05, "animation": torchAnimation, "color": '#cf2a35'});
            break;
        default:
            ui.notifications.warn("Please select a light type");
    }
  };
  }
};