/**
 * This macro is designed for the PF1e system
 * Toggles the selected actor between all languages and set languages
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

 if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {
    let 
        name = actor.getActiveTokens()[0].name,
        ALL_LANGS = Object.keys(CONFIG.PF1.languages),
        knownLangs = actor.getFlag("world","knownLangs"),
        data = {
            buttons : [[`Give all languages`, ()=>{giveall(ALL_LANGS,knownLangs)}],[`Reset`, ()=>{reset(knownLangs)}],[`Set current languages as known languages`, ()=>{setlangs()}]],       //defines the buttons used
            title : `All Languages Toggle`,   //sets the title for the button dialog
            content : `Please select an option:` //sets the text content for the button dialog
        };
    button_dialog(data);
    async function button_dialog(data) { //button function unmodified by Kekilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Dialog.js
    let value = await new Promise((resolve) => {
        let buttons = {}, dialog;

        data.buttons.forEach(([str, callback])=>{
        buttons[str] = {
            label : str,
            callback
        }
        });
    
        dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
    });
    return value;

    }
    function giveall(ALL_LANGS,knownLangs) {
        if (!knownLangs?.length) {
            ui.notifications.warn('Known languages not set! Run the set languages option first');
        } else {
            ui.notifications.info("Set "+name+" as knowing all languages");
            actor.update({"system.traits.languages.value": ALL_LANGS});
        }
    }
    function reset(knownLangs) {
        if (!knownLangs?.length) {
            ui.notifications.warn('Known languages not set! Run the set languages option first');
        } else {
            ui.notifications.info("Returned "+name+"'s languages to "+knownLangs); 
            actor.update({"system.traits.languages.value": knownLangs});
    }
    }
    function setlangs() {
        ui.notifications.info("Known languages for "+name+" as " + actor.system.traits.languages.value);
        actor.setFlag("world", "knownLangs", actor.system.traits.languages.value);
    }
};