/**
 * This macro was designed for the PF1e system and requires Noon's applyBuff macro and Advanced Macros.
 * Applies a buff named "Bleed", "Regeneration", or "Fast Healing" onto a token at an item level of input value and duration. This macro expects those buffs to have functionality from Mana's Regeneration for Pathfinder 1e module.
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

function makeCommand(buff,value,duration) {
    if (isNaN(duration)){
        let string = `Apply ${buff} at ${value}`
        return string
    } else {
        let string = `Apply ${buff} at ${value} for ${duration} rounds`
        return string
    }
}

const myTokens = canvas.tokens.controlled;
let myActors,
    preselected;
myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.isOwner);

if (!myActors.length) ui.notifications.warn("Please Select a Token");
else {
const dialog = await warpgate.menu({
    inputs: [{
        label: "Enter Value",
        type: "number"
    },{
        label: "Enter Duration",
        type: "number"
    }],
    buttons: [{
        label: 'Bleed',
        value: 1
    }, {
        label: 'Regeneration',
        value: 2
    }, {
        label: 'Fast Healing',
        value: 3
    }]
}, {
title: 'Choose Bleed/Healing Option',
options: {
    width: '100px',
    height: '100%',    
}
});
let choice = dialog['buttons'],
    input = dialog['inputs'];
if (choice === false) {
}
else {
let value = input[0],
    duration = input[1];
// duration = duration-1 //Duration offset because of current regen module issue. Likely resolved in future patch in v10.
if (isNaN(value)){
        ui.notifications.warn("Please input a value")
} else {
console.log(value)
console.log(duration)
switch(choice){
case 1:{
    let command=makeCommand("Bleed",value,duration)
    let args=["applyBuff",command]
    let retVal = game.macros.getName("callMacro (args)").execute(args);
    break;
}
case 2:{
    let command=makeCommand("Regeneration",value,duration)
    let args=["applyBuff",command]
    let retVal = game.macros.getName("callMacro (args)").execute(args);
    break;
}
case 3:{
    let command=makeCommand("Fast Healing",value,duration)
    let args=["applyBuff",command]
    let retVal = game.macros.getName("callMacro (args)").execute(args);
    break;
}
}
}
}
}