/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * A macro to add events to player logs or the adventure log
 * This macro uses DF Chat Enhancements.
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

 function writeInLog (log, gmLog) {
	if (gmLog === false) {
		AdventureLog.event(log, true)
	}
	if (gmLog === true) {
		AdventureLog.gmevent(log, true)
	}
	}
  
let d = new Dialog({
	title: 'Write in Log (GM)',
	content: `
<form>
	<fieldset>
		<legend>Input your log</legend>
	<label for="logTextog">Enter log</label>
		<textarea id="logText" name="logInfo" cols="10" autofocus="autofocus" placeholder="Enter your log here...">
		</textarea>
		<div>
			<input type="checkbox" id="gmLog" name="logInfo" value="GM Log" checked>
			<label for="gmLog">Send to GM's log?</label>
		</div>
	</fieldset>
</form>
`,
	buttons: {
		cast: {
			icon: "<i class='fas fa-check'></i>",
			label: `Confirm Log`,
		callback: async (html) => {
			console.log("Button Pushed")
			let log = html.find('[id="logText"]')[0].value;
			let gmLog = html.find('[id="gmLog"]')[0].checked;
			writeInLog(log,gmLog)
		}
		}
	},
	render: (html) => { html[0].querySelector("#logText").focus(); },
	close: html => {
		return;
	},

}).render(true)