/**
 * This macro was designed for the PF1e system and requires Noon's applyBuff macro and Warp Gate module.
 * 
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

function makeCommand(type,buff,value,duration,alias,durationType) {
    let command=`${type} ${buff}`;
    if (!(isNaN(value))){
        command = command+` at ${value}`
    };
    if (!(isNaN(duration))){
        command = command+` for ${duration} ${durationType}`
    };
    if (alias != undefined && alias != ''){
        command = command+` as ${alias}`
    }
    return command
}

async function createChatMessage(results_html){
    const chatContent = {
      content: results_html,
      speaker: ChatMessage.getSpeaker(),
      whisper: ChatMessage.getWhisperRecipients("GM")
    }
    return ChatMessage.create(chatContent)
}

const myTokens = canvas.tokens.controlled;
let myActors,
    preselected;
myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.isOwner);

if (!myActors.length) ui.notifications.warn("Please Select a Token");
else {
    const dialog = await warpgate.menu({
        inputs: [{
            label: "Enter Buff",
            type: "text",
            options: ""
        },{
            label: "Enter Value",
            type: "number",
            options: NaN
        },{
            label: "Enter Duration",
            type: "number",
            options: NaN
        },{
            label: "Duration Type",
            type: "Select",
            options: [
                { html: "Rounds", value: "Rounds", },
                { html: "Minutes", value: "Minutes", },
                { html: "Hours", value: "Hours", },
                { html: "Turns", value: "Turns", },
            ]
        },{
            label: "Alias",
            type: "text",
            options: ""
        },{
            label: "Chat?",
            type: "checkbox"
        }],
        buttons: [{
            label: 'Apply',
            value: 1
        }, {
            label: 'Toggle',
            value: 2
        }, {
            label: 'Remove',
            value: 3
        }]
    }, {
    title: 'Enter Buff Parameters',
    options: {
        width: '100px',
        height: '100%',    
    }
    });
    let choice = dialog['buttons'],
        input = dialog['inputs'];
    if (choice === false) {}
    else {
        let buff = input[0],
            value = input[1],
            duration = input[2],
            durationType = input[3],
            alias = input[4],
            chat = input[5];
        if (buff === ''){
            ui.notifications.warn("Please input a buff")
        } else {
            switch(choice){
            case 1:{
                let command=makeCommand("Apply",buff,value,duration,alias,durationType)
                let args=["applyBuff",command]
                if(chat!=false) {
                    let results_html=`@Macro[applyBuff]{${command}}`
                    createChatMessage(results_html)
                } else{ 
                    await game.macros.getName("applyBuff")?.execute({myActors,myTokens,command})
                }
                break;
            } 
            case 2:{
                let command=makeCommand("Toggle",buff)
                let args=["applyBuff",command]
                if(chat!=false) {
                    let results_html=`@Macro[applyBuff]{${command}}`
                    createChatMessage(results_html)
                } else{
                    await game.macros.getName("applyBuff")?.execute({myActors,myTokens,command})
                }
                break;
            }
            case 3:{
                let command=makeCommand("Remove",buff)
                let args=["applyBuff",command]
                if(chat!=false) {
                    let results_html=`@Macro[applyBuff]{${command}}`
                    createChatMessage(results_html)
                } else{
                    await game.macros.getName("applyBuff")?.execute({myActors,myTokens,command})
                }
                break;
            }
            }
        }
    }  
}