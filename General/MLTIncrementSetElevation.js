/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * Intended for use with Multilevel Tokens. First argument is what you want macro to do on entry, the second is what to do on exit, the third is on move. Need a character in each arguments place. E.g .,.,set:5 
 * Formatted action:number, so to increase height by 10 it would be up:10. Swap moves between two heights (: delimited) and only affects tokens starting at one of the two heights.
 * 
 * Possible actions; Up, down, set, swap
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */
function changeElevation(arg,param,tokenElev) {
    let action=arg.split(":")[0],
        num=arg.split(":")[1],
        num2=arg.split(":")[2];
    action=action.toUpperCase();
    console.log("test")
    num=Number(num);
    num2=Number(num2);
    if(isNaN(num)){
        ui.notifications.warn(param.concat(" parameter didn't work"));
    }
    else {
    switch(action){
        case "UP":
            token.update({ elevation: tokenElev+num});
            break
        case "DOWN":
            if (tokenElev-num>0){
                token.update({ elevation: tokenElev-num});
            } else{
                token.update({ elevation: 0 });
            }
            break
        case "SET":
            if (tokenElev !=num) {
            token.update({ elevation: num});
            }
            break
        case "SWAP":
            if (isNaN(num2)) {
                ui.notifications.warn("Swap requires two : delimited numbers!")
            }
            else if (tokenElev===num) {
                token.update({ elevation: num2});
            }
            else if (tokenElev===num2) {
                token.update({ elevation: num});
            }
            break
        default:
            ui.notifications.warn(param.concat(" parameter didn't work"))
    }
}}

const entryParam = args[0];
const leaveParam = args[1];
const moveParam = args[2];
const tokenElev = token.elevation;
if (event === MLT.ENTER) {
    if (typeof(entryParam)==="undefined"){
    } else {changeElevation(entryParam,"Entry",tokenElev)}
} 
if (event === MLT.LEAVE) {
    if (typeof(leaveParam)==="undefined"){
    } else {changeElevation(leaveParam,"Leave",tokenElev)}
} if (event === MLT.MOVE) {
    if (typeof(moveParam)==="undefined"){
    } else {changeElevation(moveParam,"Entry",tokenElev)}
}