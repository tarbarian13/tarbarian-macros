/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

 if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} 
else {
    
    let data = {
      buttons : [[`Reset Elevation`, ()=>{setElevation('')}],[`10 ft`, ()=>{setElevation(10)}],          //*
                  [`15 ft`, ()=>{setElevation(15)}], [`20 ft`, ()=>{setElevation(20)}], [`Custom`, ()=>{customElevation()}]],        //defines the buttons used
      title : `Change Token Elevation`,   //sets the title for the button dialog
      content : `Please select a token elevation` //sets the text content for the button dialog
  }

button_dialog(data); //calls the button dialog

async function button_dialog(data) //button function unmodified by Kekilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Dialog.js
{
  let value = await new Promise((resolve) => {
    let buttons = {}, dialog;

    data.buttons.forEach(([str, callback])=>{
      buttons[str] = {
        label : str,
        callback
      }
    });
    dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
  });
  return value;
}

function setElevation(flag) { //sets the sight limit for all selected tokens
  (async () => {
    for (let t of canvas.tokens.controlled) {
        await t.document.update({elevation:flag});
    }
})();
}
function customElevation() {
  let tokenElevation = {flag: ""},
    dialogContentlimit = `<div><span style="flex:1">Enter the custom vision limit (must be an integer): <input name="limit" style="width:350px"/></span></div>`, 
    d = new Dialog({                                                            //*
    title: "Custom token elevation",                                            //*
    content: dialogContentlimit,                                                //*
    buttons: {                                                                  //*
        done: {                                                                 //*
            label: "Confirm",                                                   //creating and defining the dialog
            callback: (html) => {
                let myElevation = html.find("[name=limit]")[0].value;           //assigns the vision limit based on input text
                tokenElevation["flag"] = myElevation                            //sets the flag parameter of sightLimt to the input string
                setElevation(parseInt((tokenElevation["flag"])))                //turns the string into a int and passes to the setElevation function
            }
        },
    },
    default: "done"    
});
d.render(true);
}
}