/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * Detaches all Token Attacher links from selected tokens.
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */
if (!token) {
	ui.notifications.warn("You must select a token!");
};

for (let t of canvas.tokens.controlled) {
	await tokenAttacher.detachAllElementsFromToken(t, suppressNotification=false);
};