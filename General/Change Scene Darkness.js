let data = 
{
    buttons : [[`0`, ()=>{setDarkness(0)}],[`.1`, ()=>{setDarkness(.1)}],          //*
                    [`.2`, ()=>{setDarkness(.2)}], [`.3`, ()=>{setDarkness(.3)}],        //defines the buttons used
                    [`.4`, ()=>{setDarkness(.4)}], [`.5`, ()=>{setDarkness(.5)}], [`.6`, ()=>{setDarkness(.6)}],[`.7`, ()=>{setDarkness(.7)}],[`.8`, ()=>{setDarkness(.8)}],[`.9`, ()=>{setDarkness(.9)}],[`1`, ()=>{setDarkness(1)}]],
    title : `Change Darkness Level`,   //sets the title for the button dialog
    content : `Please select a darkness level` //sets the text content for the button dialog
}

button_dialog(data); //calls the button dialog
  
async function button_dialog(data) //button function unmodified by Kekilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Dialog.js
{
let value = await new Promise((resolve) => {
    let buttons = {}, dialog;

    data.buttons.forEach(([str, callback])=>{
    buttons[str] = {
        label : str,
        callback
    }
    });
    dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
});
return value;
}

async function setDarkness(flag)
{
await canvas.scene.update({darkness: flag})
}