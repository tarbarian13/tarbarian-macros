// Get Rolling! 
// Request rolls from Däyvé Graves
// Abilities: 
// Saves: 
// Skills: 
function sendRoll(dice,blind,character) {

    if (blind === true) {
    let data = {
        "user": "tokens",
        "actors": [
          character[0]
        ],
        "abilities": [],
        "saves": [],
        "skills": [],
        "advantage": 0,
        "mode": "blindroll",
        "title": "Get Rolling!",
        "message": "",
        "formula": dice,
        "initiative": false,
        "tables": [],
        "chooseOne": false
      };
      
      game.socket.emit('module.lmrtfy', data);
    } else {
    let data = {
        "user": "tokens",
        "actors": [
            character[0]
        ],
        "abilities": [],
        "saves": [],
        "skills": [],
        "advantage": 0,
        "mode": "publicroll",
        "title": "Get Rolling!",
        "message": "",
        "formula": dice,
        "initiative": false,
        "tables": [],
        "chooseOne": false
        };
        
        game.socket.emit('module.lmrtfy', data);
    }
};

let input = [{label:'Enter dice type',type:'text', options:'1d12+1d100'},{label:'Blind Roll?',type:'checkbox', options:true},{label:'Dayve', type:'radio',options:'group1'},{label:'Elizabeth', type:'radio',options:'group1'},{label:'Nadja', type:'radio',options:'group1'},{label:'Aishigi', type:'radio',options:'group1'}];
const dialog = await warpgate.menu({
		inputs: input,
		buttons: [{
		  label: 'Request Roll',
		  value: 1
		}]
	  }, {
		options: {
		  width: '100px',
		  height: '100%'
		}
	  });

if (dialog['buttons'] === 1) {
    let choices = dialog['inputs'],
    button = dialog['buttons'],
    dice = choices.shift(),
    blind = choices.shift(),
    choice = choices.find(x => !!x);
    var character = {};
    switch(choice) {
        case 'Dayve':
            character[0]="osvx55twvj2dQOBW"
            sendRoll(dice,blind,character)
            break;
        case 'Elizabeth':
            character[0]="9C7209OHKglkqmZc"
            sendRoll(dice,blind,character)
            break;
        case 'Nadja':
            character[0]="UTph60oo05EzuHh7"
            sendRoll(dice,blind,character)
            break;
        case 'Aishigi':
            character[0]="gs4ieq53sdelS0NZ"
            sendRoll(dice,blind,character)
            break;    
    }
}