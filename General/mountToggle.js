/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * A macro that mounts specified rider on target mount.
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */
/**
 * CONFIGURATION:
 * Set the rider and mount parameters to the name of the tokens you want to mount/unmount. 
 * Set the familiar parameter to True or False if you want the rider to be mounted in the familiar position
 */
const mount = ""
    rider = ""
    familiar = false
/**
 * COMMAND:
 */
function mountRider(mount,rider,familiar) {
const selectedActor = game.actors.getName(rider),
    selectedToken = selectedActor.token?.object ?? selectedActor.getActiveTokens()[0],
    targetActor = game.actors.getName(mount),
    targetToken = targetActor.token?.object ?? targetActor.getActiveTokens()[0];
const options = {Familiar:familiar}

if ((selectedToken != undefined) & (targetToken != undefined)) {
    game.Rideable.MountbyID(selectedToken.id,targetToken.id,options)
}
}
function unmountRider(rider) {
const targetActor = game.actors.getName(rider),
    targetToken = targetActor.token?.object ?? targetActor.getActiveTokens()[0];
if (targetToken != undefined){
    game.Rideable.UnMountbyID(targetToken.id)
}
}

let data = 
{
    buttons : [[`Mount ${rider} on ${mount}`, ()=>{mountRider(mount,rider,familiar)}],[`Unmount ${rider} from ${mount}`, ()=>{unmountRider(rider)}]],
    title : `Mount Toggle`,   //sets the title for the button dialog
    content : `` //sets the text content for the button dialog
}

button_dialog(data); //calls the button dialog
  
async function button_dialog(data) //button function unmodified by Kekilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Dialog.js
{
let value = await new Promise((resolve) => {
    let buttons = {}, dialog;

    data.buttons.forEach(([str, callback])=>{
    buttons[str] = {
        label : str,
        callback
    }
    });
    dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
});
return value;
}