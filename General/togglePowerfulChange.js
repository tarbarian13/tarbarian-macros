// CONFIGURATION
// If one or more tokens are selected, those will be used instead of the listed actors
// Leave the actorNames array empty to guess the players
// Example actorNames: `actorNames: ["Bob", "John"],`
function toggleBuff(inputBuff) {
    const c = {
        actorNames: [],
        buffName: inputBuff
      };
      // END CONFIGURATION
      
      const tokens = canvas.tokens.controlled;
      let actors = tokens.map(o => o.actor);
      if (!actors.length && c.actorNames.length) actors = game.actors.filter(o => c.actorNames.includes(o.name));
      if (!actors.length) actors = game.actors.filter(o => o.isPC);
      actors = actors.filter(o => o.testUserPermission(game.user, "OWNER"));
      
      if (!actors.length) ui.notifications.warn("No applicable actor(s) found");
      else {
        for (const actor of actors) {
          const buff = actor.items.find(o => o.name === c.buffName && o.type === "buff");
          if (buff != null) {
            let active = getProperty(buff.data, "data.active");
            if (active == null) active = false;
            buff.update({ "data.active": !active });
          }
        }
      }
    }
    const myTokens = canvas.tokens.controlled;
    let myActors,
        preselected;
    myActors = myTokens.map(o => o.actor);
    myActors = myActors.filter(o => o.isOwner);
    
    if (!myActors.length) ui.notifications.warn(c.warningMissingActors);
    else {
    const dialog = await warpgate.menu({
        buttons: [{
            label: 'Strength',
            value: 1
        }, {
            label: 'Dexerity',
            value: 2
        }, {
            label: 'Constitution',
            value: 3
        }, {
            label: 'Intelligence',
            value: 4
        }, {
            label: 'Wisdom',
            value: 5
        }, {
            label: 'Charisma',
            value: 6
        }]
      }, {
        title: 'Choose Light Option',
        options: {
          width: '100px',
          height: '100%',    
        }
      });
      let number = dialog['buttons']
      switch(number){
        case 1:{
            let buff="Powerful Change (Strength)";
            toggleBuff(buff);
            break;
        }
        case 2:{
            let buff="Powerful Change (Dexterity)";
            toggleBuff(buff);
            break;
        }
        case 3:{
            let buff="Powerful Change (Constitution)";
            toggleBuff(buff);
            break;
        }
        case 4:{
            let buff="Powerful Change (Intelligence)";
            toggleBuff(buff);
            break;
        }
        case 5:{
            let buff="Powerful Change (Wisdom)";
            toggleBuff(buff);
            break;
        }
        case 6:{
            let buff="Powerful Change (Charisma)";
            toggleBuff(buff);
            break;
        }
    
    }
    }