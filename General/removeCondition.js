var condition = args[0],
    tokenId = args[1];
if (['Cowering', 'Frightened', 'Panicked', 'Shaken'].includes(condition)) {
    let conditionKey = 'fear',
    updateData = {};
    updateData[`data.attributes.conditions.${conditionKey}`] = false;
    canvas.tokens.get(tokenId)?.actor.update(updateData);
} else {
    var lowercaseCondition = condition.toLowerCase();
    let conditionKey = lowercaseCondition,
    updateData = {};
    updateData[`data.attributes.conditions.${conditionKey}`] = false;
    canvas.tokens.get(tokenId)?.actor.update(updateData); }