/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * A quick hack macro for finding how many more spells an arcanist can prepare
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

function appendChatMessage (results_html,level,spellMessage){
	results_html = results_html.concat(`<br><h2>${level} Level Spells</h2> ${spellMessage}`)
	return results_html
}
async function createChatMessage(results_html){
  const chatContent = {
    content: results_html,
    speaker: ChatMessage.getSpeaker(),
    whisper: ChatMessage.getWhisperRecipients("GM")
  }
  return ChatMessage.create(chatContent)
}

let results_html=""
canvas.tokens.controlled.filter(t => t.actor).map(t => t.actor).forEach(a => {
  const data = a.data.data;
  const spellbooks = data.attributes.spells.spellbooks;
  Object.entries(spellbooks).forEach(([bookId, bookData]) => {
    Object.entries(bookData.spells).forEach(([levelId, levelData]) => {
      const level = Number(levelId.replace(/^spell/, ''));
      const message = levelData.spellMessage;
      if (message!=undefined&&message!=""&&bookData.inUse===true){
        results_html=appendChatMessage(results_html,level,message)
      }
    })
  });
  if (results_html==="") {
    results_html="<h2>All spells prepared correctly</h2>"
  };
  createChatMessage(results_html)
});