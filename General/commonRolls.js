/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * A macro request some common roll types for the actors specified in the actors object.
 * This macro uses Let Me Roll That For You
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

let rolls = ['Blind Perception','Initiative'],
    input = [{label:'Select Roll',type:'select', options:rolls}];
const dialog = await warpgate.menu({
		inputs: input,
		buttons: [{
		  label: 'Request Rolls',
		  value: 1
		}]
	  }, {
		options: {
		  width: '100px',
		  height: '100%'
		}
	  });

if (dialog['buttons'] === 1) {
    let choices = dialog['inputs'],
    choice = choices[0];
    switch(choice) {
        case 'Blind Perception':
            // Get Rolling! 
            // Request rolls from Aishigi, Däyvé Graves, Elizabeth Graves, Nadja Ostrowski, Starlight
            // Abilities: 
            // Saves: 
            // Skills: Perception
            let blindRoll = {
                "user": "tokens",
                "actors": [
                "gs4ieq53sdelS0NZ",
                "osvx55twvj2dQOBW",
                "9C7209OHKglkqmZc",
                "UTph60oo05EzuHh7",
                "cAdmIwOYZ1MIrJJx"
                ],
                "abilities": [],
                "saves": [],
                "skills": [
                "per"
                ],
                "advantage": 0,
                "mode": "blindroll",
                "title": "Get Rolling!",
                "message": "",
                "formula": "",
                "initiative": false,
                "tables": [],
                "chooseOne": false
            };
            game.socket.emit('module.lmrtfy', blindRoll);
            break;
        case 'Initiative':
            let initiative = {
                "user": "tokens",
                "actors": [
                "gs4ieq53sdelS0NZ",
                "osvx55twvj2dQOBW",
                "9C7209OHKglkqmZc",
                "UTph60oo05EzuHh7"
                ],
                "abilities": [],
                "saves": [],
                "skills": [],
                "advantage": 0,
                "mode": "publicroll",
                "title": "Get Rolling!",
                "message": "",
                "formula": "",
                "initiative": true,
                "tables": [],
                "chooseOne": false
            };
            
            game.socket.emit('module.lmrtfy', initiative);
    }
}