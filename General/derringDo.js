/**  
 * Author: Tarbarian (Tarbarian#9526)
 * 
 * A macro that rolls Derring Do for the selected token. Requires a feature named Panache with uses.
 * 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 */

// Get an array of all currently selected tokens
const selectedTokens = canvas.tokens.controlled;

// If no tokens are selected, display a warning
if (selectedTokens.length === 0) {
  ui.notifications.warn("You must have a token selected to use this macro!");
}
// If more than one token is selected, display a warning
else if (selectedTokens.length > 1) {
  ui.notifications.warn("This macro only works on one token at a time!");
}
// If exactly one token is selected, display a dialog box with six buttons
else {
  const buttons = {
    acrobatics: {
      label: "Acrobatics",
      callback: () => {
        console.log("Acrobatics selected");
        derringDo("Acrobatics");
      },
    },
    climb: {
      label: "Climb",
      callback: () => {
        console.log("Climb selected");
        derringDo("Climb");
      },
    },
    escapeArtist: {
      label: "Escape Artist",
      callback: () => {
        console.log("Escape Artist selected");
        derringDo("EscapeArtist");
      },
    },
    fly: {
      label: "Fly",
      callback: () => {
        console.log("Fly selected");
        derringDo("Fly");
      },
    },
    ride: {
      label: "Ride",
      callback: () => {
        console.log("Ride selected");
        derringDo("Ride");
      },
    },
    swim: {
      label: "Swim",
      callback: () => {
        console.log("Swim selected");
        derringDo("Swim");
      },
    },
  };
  new Dialog({
    title: "Select a Skill",
    buttons: buttons,
    default: "acrobatics",
  }).render(true);
}

function derringDo(skill) {
    const selectedToken = canvas.tokens.controlled[0];
    const actor = selectedToken.actor;
    const panacheItem = actor.items.getName("Panache")
    const panache = panacheItem.system.uses.value;
    var skillSet = {Acrobatics:`acr`,Climb:`clm`,EscapeArtist:`esc`,Fly:`fly`,Ride:`rid`,Swim:`swm`}
    let skAbv = skillSet[skill]
    console.log(skAbv)
    if (!panache || panache < 1) {
      ui.notifications.warn(`${actor.name} doesn't have enough panache to use Derring-Do!`);
      return;
    }
    panacheItem.update({"system.uses.value": panache - 1});
    actor.rollSkill(skAbv, {skipDialog:true, dice:"1d20+1d6x"})
    
  }