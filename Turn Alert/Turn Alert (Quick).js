/**
 * This macro requires the Turn Alert module and is designed for the PF1e system but should work with any
 * Creates an alert that triggers after the duration expires
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 *  (based on a macro by Cole Schultz (cole#9640))
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 * For other Turn Alert example macros and api reference,
 *   visit the Turn Alert wiki:
 *   https://github.com/schultzcole/FVTT-Turn-Alert/wiki
 */
if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {
    
    var recipients = [];
    let alertData = {label: ""},
        dialogContentlabel = `<div><span style="flex:1">Alert label and message: <input name="label" style="width:350px"/></span></div>`, //text box for inputting alert name
        dialogContentpublic = `<label>Public?</label><input type="checkbox" name="public">`, //checkbox for determining if the message should be public
        dialogContentowner = `<label>To owner?</label><input type="checkbox" name="owner">`, //checkbox for determining if the message should sent to the owner of the token
        dialogContentid = `<label>On selected tokens turn?</label><input type="checkbox" name="id" checked>`, //checkbox for determining where in the turn order alert is created
        //dialogContentmessage = `<div><span style="flex:1">Message: <input name="message" style="width:350px"/></span></div>`, //text box for inputting end message
        //dialogContentduration = `<div><span style="flex:1">Duration: <input name="duration" style="width:350px"/></span></div>`, //text box for inputting duration
        conditions = [1,2,3,4,5,6,7,8,9,10],
        data = {
        buttons : conditions.map(val=> [val+" rounds", ()=>(turnAlert(val, alertData))]),       //defines the buttons used
        title : `Select Condition`,   //sets the title for the button dialog
        content : `Please select the condition:` //sets the text content for the button dialog
        },
        d = new Dialog({                                                            //*
        title: "Enter spell name",                                                  //*
        content: dialogContentlabel + dialogContentpublic + dialogContentowner + dialogContentid, //*
        buttons: {                                                                  //*
            done: {                                                                 //*
                label: "Confirm",                                                   //creating and defining the dialog
                callback: (html) => {
                    let myLabel = html.find("[name=label]")[0].value,               //assigns the name based on input text
                        isPublic = html.find("[name=public]")[0].checked,           //creates Boolean if public checkbox is checked
                        toOwner = html.find("[name=owner]")[0].checked,             //'' '' '' to owner '' '' ''
                        selectedTurnid = html.find("[name=id]")[0].checked;          //'' '' '' id '' '' ''
                    alertData.label = myLabel;        //changes the label of alertData                                 
                    alertData.roundAbsolute = false;  //sets round absolute as false, meaning the alert triggers a number of rounds after it was created
                    if (selectedTurnid) {
                        alertData.turnId = game.combat.turns.find(turn => turn.data.tokenId === token.data._id).id;   //sets the alert to be on the selected tokens turn
                        } else {
                        alertData.turnId = game.combat.combatant.id;   //sets the alert to be on the current turn
                        }
                    alertData.message = myLabel;    //sets the message
                    alertData.macro = null;   //sets the macro to be triggered on end, null for none
                    recipients = recipients.concat(game.users.filter((u) => u.isGM).map((u) => u.data._id)); //adds the GM to the list of recipients
                    if (toOwner) {
                        recipients = recipients.concat(game.users.filter(u => token.actor.hasPerm(u, CONST.ENTITY_PERMISSIONS.OWNER)).map((u) => u.data._id)); //adds the owner to recipients if to owner checked
                        }
                    if (isPublic) {
                        recipients = []; //clears recipients if public is checked, sending the message to everyone.
                        }
                    alertData.recipientIds = recipients; //adds all the recipients to the alertData object
                    //TurnAlert.create(alertData); //creates the alert
                    button_dialog(data);
                }
            },
        },
        default: "done"    
    });
    d.render(true);

    async function button_dialog(data) //button function unmodified by Keklilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Button_Dialog.js
        {
        let value = await new Promise((resolve) => {
            let buttons = {}, dialog;
        
            data.buttons.forEach(([str, callback])=>{
            buttons[str] = {
                label : str,
                callback
            }
            });
        
            dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
        });
        return value;
        }
    function turnAlert(duration, alertData) {
        alertData.round = parseInt(duration);
        TurnAlert.create(alertData);
    }
}