/**
 * This macro requires the Turn Alert module and is designed for the PF1e system
 * This macro uses another macro called removeCondition, and will not function without it
 * Creates an alert that notifies everyone when a selected condition wears off
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 * For other Turn Alert example macros and api reference,
 *   visit the Turn Alert wiki:
 *   https://github.com/schultzcole/FVTT-Turn-Alert/wiki
 */

if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {

let conditions = ['Bleed','Blind','Confused','Cowering','Dazed','Dazzled',
        'Deaf','Entangled',,'Exhausted','Fascinated','Fatigued','Frightened','Helpless','Incorporeal','Invisible','Nauseated','Panicked',
        'Paralyzed','Petrified','Shaken','Sickened','Staggered','Stunned','Unconscious'],
    data = {
        buttons : conditions.map(val=> [val, ()=>(alert(val))]),       //defines the buttons used
        title : `Select Condition`,   //sets the title for the button dialog
        content : `Please select the condition:` //sets the text content for the button dialog
    }
button_dialog(data);
async function button_dialog(data) //button function unmodified by Kekilla#7036 found here: https://github.com/Kekilla0/Personal-Macros/blob/master/Useful%20Functions/Dialog.js
{
  let value = await new Promise((resolve) => {
    let buttons = {}, dialog;

    data.buttons.forEach(([str, callback])=>{
      buttons[str] = {
        label : str,
        callback
      }
    });
  
    dialog = new Dialog({title : data.title , content : data.content, buttons, close : () => resolve(true) }).render(true);
  });
  return value;
}
function alert(condition) {
    var recipients = [];
    let alertData = {label: ""}, //creates the alertData object
        dialogContentduration = `<div><span style="flex:1">Input condition duration: <input name="duration" style="width:350px"/></span></div>`, //text box for inputting duration
        dialogContentpublic = `<label>Public?</label><input type="checkbox" name="public">`, //checkbox for determining if the message should be public
        dialogContentowner = `<label>To owner?</label><input type="checkbox" name="owner">`, //checkbox for determining if the message should sent to the owner of the token
        d = new Dialog({                                        //*
    title: "Enter Duration",                                    //*
    content: dialogContentduration +                            //*
        dialogContentowner + dialogContentpublic,               //*
    buttons: {                                                  //*
        done: {                                                 //*
            label: "Change!",                                   //creates and defines the dialog box
            callback: (html) => {
                let myDuration = html.find("[name=duration]")[0].value,             //'' '' duration '' '' '' ''
                    isPublic = html.find("[name=public]")[0].checked,            //creates Boolean if public checkbox is checked
                    toOwner = html.find("[name=owner]")[0].checked;             //'' '' '' to owner '' '' ''
                alertData.label = condition+" will expire on "+token.name;        //changes the label of alertData                                 
                alertData.round = parseInt(myDuration);                           //sets the duration of alert Data
                alertData.roundAbsolute = false;                                  //sets round absolute as false, meaning the alert triggers a number of rounds after it was created
                alertData.turnId = game.combat.combatant.id;                     //sets the alert to be on the current turn
                alertData.macro = 'removeCondition';   //sets the macro to be triggered on end, null for none
                alertData.args = [condition, token.id];
                alertData.message = condition + " on " + token.name + " has worn off."; 
                recipients = recipients.concat(game.users.filter((u) => u.isGM).map((u) => u.data._id)); //adds the GM to the list of recipients
                if (toOwner) {
                    recipients = recipients.concat(game.users.filter(u => token.actor.hasPerm(u, CONST.ENTITY_PERMISSIONS.OWNER)).map((u) => u.data._id)); //adds the owner to recipients if to owner checked
                    }
                if (isPublic) {
                    recipients = []; //clears recipients if public is checked, sending the message to everyone.
                    }
                alertData.recipientIds = recipients; //adds all the recipients to the alertData object
                applyCondition(condition);
                TurnAlert.create(alertData); //creates the alert
            }
        },
    },
    default: "done"    
    })
    d.render(true);
}
function applyCondition(condition) {
    if (['Cowering', 'Frightened', 'Panicked', 'Shaken'].includes(condition)) {
        let conditionKey = 'fear',
        updateData = {};
        updateData[`data.attributes.conditions.${conditionKey}`] = true;
        actor.update(updateData);
    } else {
        var lowercaseCondition = condition.toLowerCase(); 
        let conditionKey = lowercaseCondition,
        updateData = {};
        updateData[`data.attributes.conditions.${conditionKey}`] = true;
        actor.update(updateData);
    }
}
}