/**
 * This macro requires the Turn Alert module and is designed for the PF1e system but should work with any
 * Creates an alert that heals the selected token for a set amount at the start of each of their turns
 *  for a set duration
 *   
 *
 * Author: Tarbarian (Tarbarian#9526) 
 *  (based on a macro by Cole Schultz (cole#9640))
 * For other macros by me 
 *  visit https://gitlab.com/tarbarian13/tarbarian-macros
 * For other Turn Alert example macros and api reference,
 *   visit the Turn Alert wiki:
 *   https://github.com/schultzcole/FVTT-Turn-Alert/wiki
 */
if (!actor) {
    ui.notifications.warn('You need to select a token before using this macro!'); //ends macro and warns if token not selected
} else {

    var recipients = [];
    let alertData = {label: ""},
        dialogContentamount = `<div><span style="flex:1">Healing amount: <input name="healing" style="width:350px"/></span></div>`, //text box for inputting alert name
        dialogContentowner = `<label>To owner?</label><input type="checkbox" name="owner">`, //checkbox for determining if the message should sent to the owner of the token
        dialogContentmessage = `<label>Send message?</label><input type="checkbox" name="message" checked>`, //checkbox for determining if the message should sent to the owner of the token
        dialogContentduration = `<div><span style="flex:1">Duration (leave blank for unlimited): <input name="duration" style="width:350px"/></span></div>`, //text box for inputting duration
        d = new Dialog({                                                            //*
        title: "Enter alert info",                                                  //*
        content: dialogContentamount + dialogContentowner+ dialogContentmessage + dialogContentduration ,  //*
        buttons: {                                                                  //*
            done: {                                                                 //*
                label: "Confirm",                                                   //creating and defining the dialog
                callback: (html) => {
                    let myDuration = html.find("[name=duration]")[0].value,         //assigns the duration based on input text
                        myAmount = html.find("[name=healing]")[0].value,              //assigns the healing based on input text
                        toOwner = html.find("[name=owner]")[0].checked,              //'' '' '' to owner '' '' ''
                        sendMessage = html.find("[name=message]")[0].checked;              //'' '' '' to owner '' '' ''
                    alertData.label = "Healing "+myAmount+" per turn";    //changes the label of alertData
                    alertData.round = 0;          //set the rounds until the repeating alert triggers
                    alertData.repeating = {
                                            frequency: 1,    //sets the frequency
                                            expire: parseInt(myDuration),        //sets the duration
                                            expireAbsolute: false                //makes the alert ends a number of rounds after it was created, rather than a specific turn
                                            };
                    alertData.roundAbsolute = false; //sets round absolute as false, meaning the alert triggers a number of rounds after it was created
                    alertData.turnId = game.combat.turns.find(turn => turn.data.tokenId === token.data._id).id;   //sets the alert to be on the selected tokens turn
                    if (sendMessage) {
                        alertData.message = token.name + " healed for " + myAmount; //sets the message
                    }
                    alertData.macro = "healTokenId"; //sets the macro to be triggered on end, null for none
                    alertData.args = [token.id, parseInt(myAmount)];
                    recipients = recipients.concat(game.users.filter((u) => u.isGM).map((u) => u.data._id)); //adds the GM to the list of recipients
                    if (toOwner) {
                        recipients = recipients.concat(game.users.filter(u => token.actor.hasPerm(u, CONST.ENTITY_PERMISSIONS.OWNER)).map((u) => u.data._id)); //adds the owner to recipients if to owner checked
                        }
                    alertData.recipientIds = recipients; //adds all the recipients to the alertData object
                    TurnAlert.create(alertData); //creates the alert
                }
            },
        },
        default: "done"    
    });
    d.render(true);
}